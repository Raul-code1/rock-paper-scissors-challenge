/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      backgroundImage: {
        triangle: "url('./src/assets/images/bg-triangle.svg')",
      },
    },
    colors: {
      "scissors-gradient-from": "#ec9e0e",
      "scissors-gradient-to": "#eca922",
      "paper-gradient-from": "#4865f4",
      "paper-gradient-to": "#5671f5",
      "rock-gradient-from": "#dc2e4e",
      "rock-gradient-to": "#dd405d",
      "lizard-gradient-from": "#834fe3",
      "lizard-gradient-to": "#8c5de5",
      "cyan-from": "#40b9ce",
      "cyan-to": "#52bed1",
      "dark-text": "#3b4363",
      "score-text": "#2a46c0",
      "header-outline": "#606e85",
      "general-bg-color-from": "#1f3756",
      "general-bg-color-to": "#141539",
    },
  },
  plugins: [],
};
