import { createContext } from "react";

export interface AppContextInterface {
  score: number;
  options: GameOptions[];
  userPickedOption: GameOptions | null;
  generatedOption: GameOptions | null;
  resultMessage: "Drawn!" | "You Lose" | "You win" | null;
  increment: () => void;
  setSelectedOption(option: GameOptions): void;
  generateRandomOption(): void;
  startRound({
    userValue,
    generatedValue,
  }: {
    userValue: string;
    generatedValue: string;
  }): void;
  playAgain(): void;
}
export interface GameOptions {
  value: "rock" | "paper" | "scissors";
  svgImage: string;
  className: string;
  childClassName: string;
}

const AppContext = createContext({} as AppContextInterface);

export default AppContext;
