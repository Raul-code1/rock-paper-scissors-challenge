import { useContext, useReducer } from "react";

import AppContext, { GameOptions } from "./AppContext";
import appReducer, { AppInitialState } from "./app-reducer";
import gameOptions from "../connstants/gameOptions";

const INITIAL_STATE: AppInitialState = {
  score: 0,
  options: gameOptions,
  userPickedOption: null,
  generatedOption: null,
  resultMessage: null,
};

export function AppContextProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  const [state, dispatch] = useReducer(appReducer, INITIAL_STATE);

  /* TODO: change it or remove it */
  function increment() {
    dispatch({ type: "INCREMENT" });
  }

  function setSelectedOption(option: GameOptions) {
    dispatch({ type: "SET_SELECTED_OPTION", payload: option });
  }

  function generateRandomOption() {
    const number = Math.floor(Math.random() * 3);
    const generatedOption = state.options[number];
    dispatch({ type: "GENERATE_OPTION", payload: generatedOption });
  }

  /* TODO:  */
  function startRound({
    userValue,
    generatedValue,
  }: {
    userValue: string;
    generatedValue: string;
  }) {
    dispatch({ type: "START_ROUND", payload: { userValue, generatedValue } });
  }

  function playAgain() {
    dispatch({ type: "PLAY_AGAIN" });
  }

  return (
    <AppContext.Provider
      value={{
        ...state,
        increment,
        setSelectedOption,
        generateRandomOption,
        startRound,
        playAgain,
      }}
    >
      {children}
    </AppContext.Provider>
  );
}

export function useAppContext() {
  return useContext(AppContext);
}
