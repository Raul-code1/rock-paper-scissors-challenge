import { GameOptions } from "./AppContext";

export type AppInitialState = {
  score: number;
  options: GameOptions[];
  userPickedOption: GameOptions | null;
  generatedOption: GameOptions | null;
  resultMessage: "Drawn!" | "You Lose" | "You win" | null;
};

type AppAction =
  | { type: "INCREMENT" }
  | { type: "SET_SELECTED_OPTION"; payload: GameOptions }
  | { type: "GENERATE_OPTION"; payload: GameOptions }
  | {
      type: "START_ROUND";
      payload: { userValue: string; generatedValue: string };
    }
  | { type: "PLAY_AGAIN" };

function appReducer(state: AppInitialState, action: AppAction) {
  switch (action.type) {
    case "INCREMENT":
      return {
        ...state,
        score: state.score + 1,
      };

    case "SET_SELECTED_OPTION":
      return {
        ...state,
        userPickedOption: action.payload,
      };

    case "GENERATE_OPTION":
      return {
        ...state,
        generatedOption: action.payload,
      };

    case "START_ROUND":
      let tempResultMessage: "Drawn!" | "You Lose" | "You win" | null = null;
      let tempUserScore: number = state.score;
      const { userValue, generatedValue } = action.payload;

      if (userValue === generatedValue) {
        tempResultMessage = "Drawn!";
      }

      if (userValue === "rock" && generatedValue === "scissors") {
        tempResultMessage = "You win";
        tempUserScore += 1;
      } else if (userValue === "scissors" && generatedValue === "rock") {
        tempResultMessage = "You Lose";
      }

      if (userValue === "paper" && generatedValue === "rock") {
        tempResultMessage = "You win";
        tempUserScore += 1;
      } else if (userValue === "rock" && generatedValue === "paper") {
        tempResultMessage = "You Lose";
      }

      if (userValue === "scissors" && generatedValue === "paper") {
        tempResultMessage = "You win";
        tempUserScore += 1;
      } else if (userValue === "paper" && generatedValue === "scissors") {
        tempResultMessage = "You Lose";
      }

      return {
        ...state,
        resultMessage: tempResultMessage,
        score: tempUserScore,
      };

    case "PLAY_AGAIN":
      return {
        ...state,
        userPickedOption: null,
        generatedOption: null,
        resultMessage: null,
      };

    default:
      return state;
  }
}

export default appReducer;
