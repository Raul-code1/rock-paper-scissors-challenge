export { default as Duel } from "./Duel";
export { default as PickOption } from "./PickOption";
export { default as Rules } from "./Rules";
export { default as ScoreSection } from "./ScoreSection";
