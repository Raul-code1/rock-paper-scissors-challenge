import { useAppContext } from "../context/AppContextProvider";

export default function PickOption() {
  const { options, setSelectedOption } = useAppContext();

  const stylesPositions = {
    scissors: "absolute  bottom-0 right-1/4",
    paper: " absolute top-0 right-0 -translate-y-10 translate-x-10",
    rock: "  absolute top-0 -translate-y-10 -translate-x-10",
  };

  return (
    <section className="section-game  ">
      <div className="bg-triangle  bg-no-repeat bg-contain h-[230px] w-[270px]  md:h-[320px] md:w-[340px] relative ">
        {/* TODO:refactor, option card component */}
        {options.map((option, index) => {
          const extraStyles = stylesPositions[option.value];
          return (
            <div
              onClick={() => setSelectedOption(option)}
              key={index}
              className={`${option.className} ${extraStyles}`}
            >
              <div className={option.childClassName}>
                <div
                  className="bg-[white] rounded-full h-full w-full flex justify-center items-center  "
                  style={{
                    boxShadow: " inset 39px 37px 39px -47px   rgba(0,0,0,0.75)",
                  }}
                >
                  <img src={option.svgImage} alt={option.value} />
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </section>
  );
}
