import { useEffect } from "react";
import { GameOptions } from "../context/AppContext";
import { useAppContext } from "../context/AppContextProvider";
import { motion } from "framer-motion";

type Props = {
  option: GameOptions;
};

export default function Duel({ option }: Props) {
  const { generatedOption, startRound, resultMessage, playAgain } =
    useAppContext();

  useEffect(() => {
    if (generatedOption) {
      startRound({
        userValue: option.value,
        generatedValue: generatedOption.value,
      });
    }
  }, [option, generatedOption]);

  return (
    <section className="section-game">
      <div className=" h-[230px] w-[100%]  md:h-[320px] md:w-[600px]  flex justify-between items-center">
        {/*  */}
        <div className={option.className}>
          <h1 className="text-center hidden md:block mb-5 uppercase text-[white]">
            You picked
          </h1>
          <motion.div
            initial={{ x: -200, opacity: 0 }}
            animate={{ x: 0, opacity: 1 }}
            transition={{ duration: 1 }}
            className={option.childClassName}
          >
            <div
              className="bg-[white] rounded-full h-full w-full flex justify-center items-center  "
              style={{
                boxShadow: " inset 39px 37px 39px -47px   rgba(0,0,0,0.75)",
              }}
            >
              <img src={option.svgImage} alt={option.value} />
            </div>
          </motion.div>
          <h1 className="text-center  md:hidden mt-5 uppercase text-[white]">
            You picked
          </h1>
        </div>
        {/*  */}
        {generatedOption && (
          <div className="">
            <div className={generatedOption.className}>
              <h1 className="text-center hidden md:block mb-5 uppercase text-[white]">
                The house picked
              </h1>
              <motion.div
                initial={{ x: 200, opacity: 0 }}
                animate={{ x: 0, opacity: 1 }}
                transition={{ duration: 1 }}
                className={generatedOption.childClassName}
              >
                <div
                  className="bg-[white] rounded-full h-full w-full flex justify-center items-center  "
                  style={{
                    boxShadow: " inset 39px 37px 39px -47px   rgba(0,0,0,0.75)",
                  }}
                >
                  <img
                    src={generatedOption.svgImage}
                    alt={generatedOption.value}
                  />
                </div>
              </motion.div>
              <h1 className="text-center  md:hidden mt-5 uppercase text-[white]">
                The house picked
              </h1>
            </div>
          </div>
        )}
      </div>
      {/*  */}
      {resultMessage && (
        <div className="text-center  md:-translate-y-36 ">
          <p className="uppercase text-[white] text-4xl"> {resultMessage} </p>
          <button
            className="bg-[white] px-12 mt-2 py-2 rounded-lg text-dark-text transition-all hover:text-rock-gradient-to"
            onClick={playAgain}
          >
            Play again
          </button>
        </div>
      )}
    </section>
  );
}
