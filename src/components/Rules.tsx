import { useState } from "react";
import { Dialog } from "primereact/dialog";

import RulesImg from "../assets/images/image-rules.svg";

export default function Rules() {
  const [visible, setVisible] = useState(false);

  return (
    <div className="fixed  bottom-4 right-1/3 md:bottom-8  md:right-16  flex ">
      <button
        className="border-header-outline border-2 text-[white] px-8 md:px-10 py-1 rounded-lg text-2xl border-solid"
        onClick={() => setVisible(true)}
      >
        Rules
      </button>
      <Dialog
        header="Rules"
        visible={visible}
        modal={false}
        className="w-[90vw] h-[67vh] md:h-[50vh]  md:w-[30vw]  "
        onHide={() => setVisible(false)}
        draggable={false}
      >
        <img
          src={RulesImg}
          alt="Rules Image"
          className=" h-[80%] pt-14 md:pt-0 w-[80%] md:w-[70%] md:h-[100%] mx-auto"
        />
      </Dialog>
    </div>
  );
}
