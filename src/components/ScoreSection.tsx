import Logo from "../assets/images/logo.svg";
import { useAppContext } from "../context/AppContextProvider";

export default function ScoreSection() {
  const { score } = useAppContext();

  return (
    <section className="border-header-outline flex items-center justify-between p-4 rounded-xl border-[3px] border-solid  ">
      <div className="w-[32%] md:w-[25%]">
        <img
          className="w-full h-full"
          src={Logo}
          alt="Rock Paper Scissors Logo"
        />
      </div>
      <div className="bg-[white]  rounded-xl text-center   w-[30%] md:w-[18%] md:py-3">
        <p className="uppercase  text-score-text ">Score</p>
        <p className="text-5xl font-[600] text-dark-text ">{score}</p>
      </div>
    </section>
  );
}
