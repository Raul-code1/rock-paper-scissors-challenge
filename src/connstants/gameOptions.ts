import { GameOptions } from "../context/AppContext";

import Rock from "../assets/images/icon-rock.svg";
import Paper from "../assets/images/icon-paper.svg";
import Scissors from "../assets/images/icon-scissors.svg";

const gameOptions: GameOptions[] = [
  {
    value: "rock",
    svgImage: Rock,
    className: " cursor-pointer  ",
    childClassName:
      "bg-gradient-to-r  from-rock-gradient-from to-rock-gradient-to  w-36  h-36 md:h-44 md:w-44 bg-[white] rounded-full  p-4 md:p-6 ",
  },
  {
    value: "paper",
    svgImage: Paper,
    className: " cursor-pointer    ",
    childClassName:
      " bg-gradient-to-r  from-paper-gradient-from to-paper-gradient-to  w-36  h-36 md:h-44 md:w-44 bg-[white] rounded-full  p-4 md:p-6 ",
  },
  {
    value: "scissors",
    svgImage: Scissors,
    className: " cursor-pointer  ",
    childClassName:
      " bg-gradient-to-r  from-scissors-gradient-from to-scissors-gradient-to  w-36  h-36 md:h-44 md:w-44 bg-[white] rounded-full  p-4 md:p-6  ",
  },
];

export default gameOptions;
