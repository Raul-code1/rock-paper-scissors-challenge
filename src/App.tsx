import { useEffect } from "react";
import { ScoreSection, PickOption, Duel, Rules } from "./components";
import { useAppContext } from "./context/AppContextProvider";

export default function App() {
  const { userPickedOption, generateRandomOption } = useAppContext();

  useEffect(() => {
    if (userPickedOption) {
      generateRandomOption();
    }
  }, [userPickedOption]);

  return (
    <main className="w-[90%] md:w-[45%] pt-4 min-h-screen  mx-auto ">
      <ScoreSection />
      {!userPickedOption && <PickOption />}
      {userPickedOption && <Duel option={userPickedOption} />}
      <Rules />
    </main>
  );
}
